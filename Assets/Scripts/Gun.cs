﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class Gun : MonoBehaviour
{
    public SteamVR_Action_Boolean fireAction;
    public GameObject bullet;
    public Transform barrelPivot;
    public float shootingSpeed = 1;
    public ParticleSystem muzzleFlash;
    public Animator boltAnim;
    public string fireAnimation;
    public float fireRate = 0.09F;

    private Animator animator;
    private Interactable interactable;

    private AudioSource gunShotAudio;
    
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        interactable = GetComponent<Interactable>();
        gunShotAudio = GetComponent<AudioSource>();
        boltAnim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if(interactable.attachedToHand != null)
        {
            SteamVR_Input_Sources source = interactable.attachedToHand.handType;

            if(fireAction[source].stateDown)                           
            {
                Fire();
                gunShotAudio.Play();
                boltAnim.Play(fireAnimation, -1, 0f);                
                
            }

        }
    }

    void Fire()
    {
        Debug.Log("Fire");
        Rigidbody bulletrb = Instantiate(bullet, barrelPivot.position, barrelPivot.rotation * Quaternion.Euler(0f, 90f, 90f)).GetComponent<Rigidbody>();
        bulletrb.velocity = barrelPivot.forward * shootingSpeed;
        muzzleFlash.Emit(1);
                
    }     
}
